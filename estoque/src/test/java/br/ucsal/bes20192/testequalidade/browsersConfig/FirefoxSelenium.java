package br.ucsal.bes20192.testequalidade.browsersConfig;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import br.ucsal.bes20192.testequalidade.jbehave.JBehaveConfig;

public class FirefoxSelenium {
	
	
	private static final String DRIVER = "webdriver.gecko.driver";
	public static final String PATH_DRIVER = "C:\\Users\\msgbr\\Desktop\\estoque-2018-2\\estoque\\drivers\\";
	private static final String DRIVER_LOCATION_FIREFOX = "geckodriver.exe";

	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty(DRIVER, PATH_DRIVER + DRIVER_LOCATION_FIREFOX);
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.setCapability("marionette", true);
		WebDriver driver = new FirefoxDriver(firefoxOptions);
		return new InstanceStepsFactory(JBehaveConfig.configuration(), new CalcularTotalSteps(driver));
	}



}
