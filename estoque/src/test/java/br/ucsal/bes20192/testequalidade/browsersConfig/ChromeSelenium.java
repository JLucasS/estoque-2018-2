package br.ucsal.bes20192.testequalidade.browsersConfig;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.chrome.ChromeDriver;

import br.ucsal.bes20192.testequalidade.jbehave.JBehaveConfig;

public class ChromeSelenium {
	private static final String DRIVER = "webdriver.chrome.driver";
	private static final String DRIVER_LOCATION_CHROME = "chromedriver.exe";
	public static final String PATH_DRIVER = "C:\\Users\\msgbr\\Desktop\\estoque-2018-2\\estoque\\drivers\\";
	
	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty(DRIVER, PATH_DRIVER + DRIVER_LOCATION_CHROME);
		ChromeDriver driver = new ChromeDriver();
		return new InstanceStepsFactory(JBehaveConfig.configuration(), new CalcularTotalSteps(driver));
	}
}
