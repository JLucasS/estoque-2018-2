package br.ucsal.bes20192.testequalidade.jbehave;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class JBehaveSteps {

	private static WebDriver driver;
	private WebElement element;

	private static final String XPATH_SELECT = "//td[2]/select[1]";
	private static final String XPATH_JACA = "//td[2]/select[1]/option[2]";
	private static final String XPATH_MANGA = "//td[2]/select[1]/option[3]";
	
	@BeforeClass
	public static void setup(){
	System.setProperty("webdriver.chrome.driver","C:\\Users\\joao-\\git\\estoque-2018-2-joao-e-marcio\\estoque\\drivers\\chromedriver.exe");
	driver = new ChromeDriver();
	
	System.setProperty("webdriver.gecko.driver","path/geckodriver.exe");
	System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
	DesiredCapabilities capabilities = DesiredCapabilities.firefox();
	capabilities.setCapability("marionette", true);
	driver = (WebDriver) new FirefoxDriver(capabilities);

	}
	
	@AfterClass
	public static void teardown(){
	driver.quit();
	}

	


	@Given("estou na lista de compras")
	public void abrirPagina() {

	}

	@When("seleciono o produto $produto")
	public void selectProduct(String produto) {
		element = driver.findElement(By.xpath(XPATH_SELECT));
		element.click();
		switch (produto) {
		case "Jaca":
			element = driver.findElement(By.xpath(XPATH_JACA));
			element.click();
			break;
		case "Manga":
			element = driver.findElement(By.xpath(XPATH_MANGA));
			element.click();
			break;

		}
	}

	@When("informo a quantidade $quantidade")
	public void informQntd(Integer quantidade) {
		element = driver.findElement(By.id("quantidade"));
		element.sendKeys(quantidade.toString());

	}

	@When("informo o valor unit�rio $valorUnitario reais")
	public void informaValue(Integer valorUnitario) {
		element = driver.findElement(By.id("valor"));
		element.sendKeys(valorUnitario.toString());
	}

	@When("confirmo a compra")
	public void confirmBuy() {
		element = driver.findElement(By.id("calcularBtn"));
		element.click();
	}

	@Then("terei de pagar $valorTotal reais")
	public void verifyPrice(Integer valorTotal) {
		element = driver.findElement(By.id("valorTotal"));
		Integer valorAtual = Integer.parseInt("valorTotal");
		Assert.assertEquals(valorAtual, valorAtual);

	}

}
